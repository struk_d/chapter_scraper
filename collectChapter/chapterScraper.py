# pip install requests, beautifulsoup4, lxml
import csv
import datetime
import json
import logging
import sys

import requests
from requests import Session
from bs4 import BeautifulSoup

from settings import FIELDS_ORDER, FILE_NAME

BASE_URL = 'https://www.tke.org'
ALL_CHAPTER_URL = 'https://www.tke.org/find-a-chapter'
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 " \
             "Safari/537.36 "


logging.basicConfig(
    format="%(asctime)s %(levelname)s:%(name)s: %(message)s",
    level=logging.INFO,
    datefmt="%H:%M:%S",
    stream=sys.stdout,
)
logger = logging.getLogger('CS')


def get_html(session: Session, url: str) -> str:
    try:
        response = session.get(url)
        response.raise_for_status()
        text = response.text
        logger.info(f'got response from {url}')
    except requests.RequestException:
        logger.error(f'response from {url} failed')
        text = ''
    return text


def collect_data(item: list) -> dict:
    chapter_name = item[1]
    chapter_latitude = item[2]
    chapter_longitude = item[3]
    chapter_link = f'{BASE_URL}{item[4].split("=")[1].removesuffix("target")}'
    return {
        'chapter_name': chapter_name,
        'chapter_latitude': chapter_latitude,
        'chapter_longitude': chapter_longitude,
        'chapter_link': chapter_link,
    }


def json_from_text(text: str) -> list:
    text = text.partition('=')[2].strip().removesuffix(';')
    text = text.replace('\n', '').replace('\r', '').replace('"', '') \
        .replace(' ', '').replace("'", '"').replace(',]', ']')
    return json.loads(text)


def get_main_data(text: str) -> list[dict]:
    collected_data = []
    try:
        soup = BeautifulSoup(text, 'lxml')
        script_text = soup.find('div', class_="container content").find('script').text
        var = script_text.split('var')
        chapter_active = json_from_text(var[11])
        chapter_non_active = json_from_text(var[12])
        # alumi_active = json_from_text(var[13])
        # alumi_non_active = json_from_text(var[14].partition('function')[0].strip())
        for item in chapter_active:
            data = collect_data(item)
            data['is_active'] = 1
            collected_data.append(data)
        for item in chapter_non_active:
            data = collect_data(item)
            data['is_active'] = 0
            collected_data.append(data)
    except Exception:
        logger.error(f'error while collecting main data was occurred')
        exit(1)
    logger.info(f'collected {len(collected_data)} chapters')
    return collected_data


def parse_chapter_page(session: Session, url: str, is_active: int) -> dict:
    html = get_html(session, url)
    soup = BeautifulSoup(html, 'lxml')
    university = soup.find('div', class_="panel-heading").get_text(strip=True)
    items = soup.find('tbody').find_all('tr')
    if is_active:
        founded = items[0].find_all('td')[-1].text
        city = items[1].find_all('td')[-1].text
        state = items[2].find_all('td')[-1].text
        zip_code = items[3].find_all('td')[-1].text
        phone = items[4].find_all('td')[-1].text
        website = items[5].find('a').get('href')
        president = items[6].find_all('td')[-1].text
        chairman = items[7].find_all('td')[-1].text
        chapter_advisor = items[8].find_all('td')[-1].text
        e_mail = items[9].find('a').get('href')
        membership_container = soup.find('div', class_='headline').find_next_sibling('div')
        achievements_container = soup.find('div', class_='headline') \
            .find_next_sibling('div', class_='headline').find_next_sibling('div')

    else:
        founded = items[1].find_all('td')[-1].text
        city = ''
        state = ''
        zip_code = ''
        phone = ''
        website = ''
        president = ''
        chairman = ''
        chapter_advisor = ''
        e_mail = items[0].find('a').get('href')
        try:
            membership_container = soup.find_all('div', class_="counters col-md-3 col-sm-3")[0]
        except IndexError:
            membership_container = ''
        try:
            achievements_container = soup.find_all('div', class_="counters col-md-3 col-sm-3")[1]
        except IndexError:
            achievements_container = ''

    if membership_container:
        membership_titles_container = membership_container.find_all('h3')
        membership_titles = []
        for title in membership_titles_container:
            text = title.text.translate(str.maketrans('\r', ' ', '\n '))
            membership_titles.append(text)

        membership_value_container = membership_container.find_all('span', class_='counter')
        membership_values = []
        for value in membership_value_container:
            text = value.text
            membership_values.append(text)
        membership = dict(zip(membership_titles, membership_values))
    else:
        membership = ''

    if achievements_container:
        achievements_titles_container = achievements_container.find_all('h3')
        achievements_titles = []
        for title in achievements_titles_container:
            text = title.text.translate(str.maketrans('\r', ' ', '\n '))
            achievements_titles.append(text)

        achievements_value_container = achievements_container.find_all('span', class_='counter')
        achievements_values = []
        for value in achievements_value_container:
            text = value.text
            achievements_values.append(text)
        achievements = dict(zip(achievements_titles, achievements_values))
    else:
        achievements = ''

    return {'university': university,
            'founded': founded,
            'city': city,
            'state': state,
            'zip_code': zip_code,
            'phone': phone,
            'website': website,
            'president': president,
            'chairman': chairman,
            'chapter_advisor': chapter_advisor,
            'e_mail': e_mail,
            'membership': membership,
            'achievements': achievements}


def get_additional_info(session: Session, collected_data: list[dict]) -> list[dict]:
    total_pages = len(collected_data) + 1
    current = 1
    for item in collected_data:
   
        url = item['chapter_link']
        try:
            additional_info = parse_chapter_page(session, url, item['is_active'])
        except Exception:
            logger.error(
                f'{current} of {total_pages}: error while collecting additional info for {url} was occurred')
        else:
            item.update(additional_info)
            logger.info(f'{current} of {total_pages}: all data from {url} collected successfully')
        finally:
            current += 1

    return collected_data


def write_to_tsv(data: list[dict], fieldnames: list[str]) -> None:
    filename = f'{FILE_NAME}{datetime.datetime.now().strftime("%Y%d%m%H%M")}.tsv'
    with open(filename, 'w', encoding='utf8', newline='') as tsv_file:
        tsv_writer = csv.DictWriter(tsv_file, delimiter='\t', lineterminator='\n', fieldnames=fieldnames)
        tsv_writer.writeheader()
        for item in data:
            filtered_dict = {key: value for key, value in item.items() if key in FIELDS_ORDER}
            tsv_writer.writerow(filtered_dict)


def main():
    with Session() as session:
        session.headers.update({'user-agent': USER_AGENT})
        page = get_html(session, ALL_CHAPTER_URL)
        collected_data = get_main_data(page)
        all_data = get_additional_info(session, collected_data)

    write_to_tsv(data=all_data, fieldnames=FIELDS_ORDER)


if __name__ == '__main__':
    main()
